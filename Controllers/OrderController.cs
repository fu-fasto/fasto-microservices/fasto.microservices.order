﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Orders.Commands;
using FastO.Microservices.Orders.Queries;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.Orders.Controllers
{
    [ApiController]
    [Route("orders")]
    public class OrderController : ControllerBase
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public OrderController(ICommandBus commandBus, IQueryBus queryBus)
        {
            _commandBus = commandBus;
            _queryBus = queryBus;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllOrders([FromQuery] GetAllOrders query,
            CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneOrder([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetOneOrder() {Id = id}, cancellationToken);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> MakeOrder([FromBody] MakeOrder command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }

        [HttpPatch()]
        public async Task<IActionResult> UpdateOrderState([FromBody] UpdateOrderState command,
            CancellationToken cancellationToken)
        {
            var id = command.Id;
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(id);
        }
    }
}