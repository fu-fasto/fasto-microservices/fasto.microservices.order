﻿using System;
using FastO.Microservices.Orders.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.Orders.Queries
{
    public class GetOneOrder : OneQueryBase<Order, Guid>
    {
        
    }
}