﻿using System;
using FastO.Microservices.Orders.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.Orders.Queries
{
    public class GetAllOrders : AllQueryBase<Order>
    {
        public Guid? StoreId { get; set; }
        public Guid? CustomerId { get; set; }
        public Guid? VisitId { get; set; }
        public Guid? ItemId { get; set; }
    }
}