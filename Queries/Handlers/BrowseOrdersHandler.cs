﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Orders.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.Orders.Queries.Handlers
{
    public class BrowseOrdersHandler : IQueryHandler<BrowseOrders, PagedResult<Order>>
    {
        private readonly IRepository<Order, Guid> _orderRepository;

        public BrowseOrdersHandler(IRepository<Order, Guid> orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public Task<PagedResult<Order>> Handle(BrowseOrders query, CancellationToken cancellationToken)
        {
            var findQuery = query.MapTo<FindQuery<Order>>();
            var rawDict =
                new Dictionary<string, Expression<Func<Order, object>>>(StringComparer.OrdinalIgnoreCase)
                {
                    {"Quantity", x => x.Quantity},
                    {"OrderTime", x => x.OrderTime}
                };

            query.RawSorts?.ForEach(rawSort =>
            {
                if (rawDict.TryGetValue(rawSort.Field, out var selector))
                    findQuery.SortExpressions.Add(new SortExpression<Order>
                    {
                        Selector = selector,
                        IsAscending = rawSort.IsAscending
                    });
            });

            return _orderRepository.BrowseAsync(findQuery, cancellationToken);
        }
    }
}