﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Orders.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.Orders.Queries.Handlers
{
    public class GetAllOrdersHandlers : IQueryHandler<GetAllOrders, IEnumerable<Order>>
    {
        private readonly IRepository<Order, Guid> _orderRepository;

        public GetAllOrdersHandlers(IRepository<Order, Guid> orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public Task<IEnumerable<Order>> Handle(GetAllOrders query, CancellationToken cancellationToken)
        {
            var findQuery = query.MapTo<FindQuery<Order>>();

            if (query.ItemId.HasValue)
                findQuery.Filters.Add(x => x.ItemId == query.ItemId);
            if (query.VisitId.HasValue)
                findQuery.Filters.Add(x => x.VisitId == query.VisitId);

            return _orderRepository.FindAsync(findQuery, cancellationToken);
        }
    }
}