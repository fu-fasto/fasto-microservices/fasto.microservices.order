﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Orders.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.Orders.Queries.Handlers
{
    public class GetOneOrderHandler : IQueryHandler<GetOneOrder, Order>
    {
        private readonly IRepository<Order, Guid> _orderRepository;

        public GetOneOrderHandler(IRepository<Order, Guid> orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public Task<Order> Handle(GetOneOrder query, CancellationToken cancellationToken)
        {
            var order = _orderRepository.FindOneAsync(query.Id, cancellationToken)
                        ?? throw new NullReferenceException($"Order {query.Id} does not exist!");

            return order;
        }
    }
}