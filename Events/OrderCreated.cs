﻿using System;
using FastO.Microservices.Orders.DataModels;
using MicroBoost.Events;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.Orders.Events
{
    [Message]
    public class OrderCreated : IEvent
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();
        
        public Guid StoreId { get; set; }
        
        public Guid CustomerId { get; set; }
        
        public Guid VisitId { get; set; }
        
        public Guid ItemId { get; set; }
        
        public string Name { get; set; }
        
        public decimal Price { get; set; }
        
        public int Quantity { get; set; }

        public OState State = OState.Created;
        
        public DateTimeOffset ExpectedTime { get; set; }
        
        public string Note { get; set; }
    }
}