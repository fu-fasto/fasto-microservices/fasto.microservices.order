﻿using System;
using FastO.Microservices.Orders.DataModels;
using MicroBoost.Events;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.Orders.Events
{
    [Message]
    public class OrderUpdated : IEvent
    {
        [MessageKey] public Guid Id { get; set; }

        public Guid StoreId { get; set; }

        public Guid CustomerId { get; set; }

        public OState State { get; set; }

        public string Note { get; set; }
    }
}