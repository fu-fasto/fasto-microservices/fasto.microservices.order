﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Orders.Dtos;
using MicroBoost.Http;

namespace FastO.Microservices.Orders.Services
{
    public class ServiceClient : IServiceClient
    {
        private readonly IHttpClient _httpClient;
        private readonly HttpClientOptions _clientOptions;

        public ServiceClient(IHttpClient httpClient, HttpClientOptions clientOptions)
        {
            _httpClient = httpClient;
            _clientOptions = clientOptions;
        }

        public Task<ItemDto> GetItemById(Guid itemId, CancellationToken cancellationToken = default)
        {
            return _httpClient
                .GetAsync<ItemDto>(
                    $"{_clientOptions.Services["StoreSearchMicroservice"]}/items/{itemId}",
                    cancellationToken);
        }
    }
}