﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Orders.Dtos;

namespace FastO.Microservices.Orders.Services
{
    public interface IServiceClient
    {
        Task<ItemDto> GetItemById(Guid itemId, CancellationToken cancellationToken = default);
    }
}