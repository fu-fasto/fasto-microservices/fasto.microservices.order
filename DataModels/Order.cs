﻿using System;
using System.Collections.Generic;
using System.Linq;
using MicroBoost.Types;

namespace FastO.Microservices.Orders.DataModels
{
    public class Order : EntityBase<Guid>
    {
        /// <summary>
        /// Id of order
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Id of store
        /// </summary>
        public Guid StoreId { get; set; }

        /// <summary>
        /// Id of customer
        /// </summary>
        public Guid CustomerId { get; set; }

        /// <summary>
        /// Id of visit
        /// </summary>
        public Guid VisitId { get; set; }

        /// <summary>
        /// Id of item
        /// </summary>
        public Guid ItemId { get; set; }

        /// <summary>
        /// Name of item
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Price of item
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Quantity of order
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Order time
        /// </summary>
        public DateTimeOffset OrderTime { get; set; }

        /// <summary>
        /// Current state of order
        /// </summary>
        public OState State { get; set; }

        /// <summary>
        /// Current note of order
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// History of order states
        /// </summary>
        public IList<OrderState> States { get; set; } = new List<OrderState>();

        public void UpdateOrderState(OState state, string note = null)
        {
            if (States.Any() && States.Last().State == state)
            {
                Note = note;

                if (States.Any() && state != OState.Created) return;
            }

            States.Add(new OrderState()
            {
                State = state
            });
            State = state;
        }
    }
}