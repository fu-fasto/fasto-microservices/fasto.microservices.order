﻿using MongoDB.Bson.Serialization;

namespace FastO.Microservices.Orders.DataModels
{
    public class MongoStructure
    {
        public MongoStructure()
        {
            BsonClassMap.RegisterClassMap<Order>(x =>
            {
                x.AutoMap();
                x.GetMemberMap(m => m.StoreId).SetElementName("store_id");
                x.GetMemberMap(m => m.CustomerId).SetElementName("customer_id");
                x.GetMemberMap(m => m.ItemId).SetElementName("item_id");
                x.GetMemberMap(m => m.VisitId).SetElementName("visit_id");
                x.GetMemberMap(m => m.Name).SetElementName("name");
                x.GetMemberMap(m => m.Price).SetElementName("price");
                x.GetMemberMap(m => m.Quantity).SetElementName("quantity");
                x.GetMemberMap(m => m.OrderTime).SetElementName("order_time");
                x.GetMemberMap(m => m.State).SetElementName("state");
                x.GetMemberMap(m => m.Note).SetElementName("note");
                x.GetMemberMap(m => m.States).SetElementName("states");
            });
            
            BsonClassMap.RegisterClassMap<OrderState>(x =>
            {
                x.AutoMap();
                x.GetMemberMap(m => m.State).SetElementName("state");
                x.GetMemberMap(m => m.CreatedTime).SetElementName("created_time");
            });
        }
    }
}