﻿using System;

namespace FastO.Microservices.Orders.DataModels
{
    public class OrderState
    {
        /// <summary>
        /// State
        /// </summary>
        public OState State { get; set; }
        
        /// <summary>
        /// Created time
        /// </summary>
        public DateTimeOffset CreatedTime { get; set; } = DateTimeOffset.UtcNow;
    }
    
    public enum OState
    {
        Created = 0,
        Accepted = 1,
        Making = 2,
        Done = 3,
        Served = 4,
        Rejected = 5,
        Cancelled = 6
    }
}