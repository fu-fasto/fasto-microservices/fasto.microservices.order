﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Orders.DataModels;
using FastO.Microservices.Orders.Events;
using FastO.Microservices.Orders.Services;
using FluentValidation;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.Orders.Commands.Handlers
{
    public class MakeOrderHandler : ICommandHandler<MakeOrder>
    {
        private readonly ILogger<MakeOrderHandler> _logger;
        private readonly IMessagePublisher _messagePublisher;
        private readonly IRepository<Order, Guid> _orderRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceClient _serviceClient;

        public MakeOrderHandler(ILogger<MakeOrderHandler> logger, IUnitOfWork unitOfWork,
            IMessagePublisher messagePublisher, IServiceClient serviceClient)
        {
            _orderRepository = unitOfWork.GetRepository<Order, Guid>();
            _unitOfWork = unitOfWork;
            _messagePublisher = messagePublisher;
            _serviceClient = serviceClient;
            _logger = logger;
        }

        public async Task PreHandle(MakeOrder command, CancellationToken cancellationToken)
        {
            var item = await _serviceClient.GetItemById(command.ItemId, cancellationToken) ??
                       throw new ValidationException($"Item {command.ItemId} not found.");
            command.Name = item.Name;
            command.Price = item.Price;
        }

        public async Task Handle(MakeOrder command, CancellationToken cancellationToken)
        {
            var order = command.MapTo<Order>();
            order.UpdateOrderState(OState.Created, command.Note);

            using var transaction = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                await _orderRepository.AddAsync(order, cancellationToken);
                await _messagePublisher.PublishOutBoxAsync(order.MapTo<OrderCreated>(), cancellationToken);
                await transaction.CommitAsync(cancellationToken);

                _logger.LogInformation("Order {Id} created successfully!", order.Id);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync(cancellationToken);
                throw;
            }
        }
    }
}