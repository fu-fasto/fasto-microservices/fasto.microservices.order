﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Orders.DataModels;
using FastO.Microservices.Orders.Events;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.Orders.Commands.Handlers
{
    public class UpdateOrderStateHandler : ICommandHandler<UpdateOrderState>
    {
        private readonly ILogger _logger;
        private readonly IMessagePublisher _messagePublisher;
        private readonly IRepository<Order, Guid> _orderRepository;
        private readonly IUnitOfWork _unitOfWork;
        
        public UpdateOrderStateHandler(ILogger<UpdateOrderStateHandler> logger, IUnitOfWork unitOfWork,
            IMessagePublisher messagePublisher)
        {
            _orderRepository = unitOfWork.GetRepository<Order, Guid>();
            _unitOfWork = unitOfWork;
            _messagePublisher = messagePublisher;
            _logger = logger;
        }

        public async Task Handle(UpdateOrderState command, CancellationToken cancellationToken)
        {
            var order = await _orderRepository.FindOneAsync(command.Id, cancellationToken);
            order.UpdateOrderState(command.State, command.Note);

            using var transaction = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                await _orderRepository.UpdateAsync(order, cancellationToken);
                await _messagePublisher.PublishOutBoxAsync(order.MapTo<OrderUpdated>(), cancellationToken);
                await transaction.CommitAsync(cancellationToken);
                _logger.LogInformation("Order {Id} updated successfully!", order.Id);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync(cancellationToken);
                throw;
            }
        }
    }
}