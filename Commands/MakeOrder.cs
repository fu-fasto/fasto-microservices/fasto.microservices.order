﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.Orders.Commands
{
    public class MakeOrder : CommandBase
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();
        
        public Guid StoreId { get; set; }
        
        public Guid CustomerId { get; set; }
        
        public Guid VisitId { get; set; }
        
        public Guid ItemId { get; set; }
        
        public string Name { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }
        
        public DateTimeOffset OrderTime { get; set; } = DateTimeOffset.Now;
        
        public string Note { get; set; }
    }
}