﻿using System;
using FastO.Microservices.Orders.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.Orders.Commands
{
    public class UpdateOrderState : CommandBase
    {
        [MessageKey] public Guid Id { get; set; }
        
        public OState State { get; set; }
        
        public string Note { get; set; }
    }
}